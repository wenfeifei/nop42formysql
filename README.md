# nop42formysql

#### 介绍
nopcommerce 4.2 mysql 版，当然也支持sql server，添加了 vue 手机端， 添加了 仿淘宝 皮肤 主题。

#### 软件架构
软件架构说明


#### 安装教程

1. 在vs2017下直接调试或者运行，或者发布后建立建店运行
2. 出现安装界面，选择mysql,填写好连接串
3. xxxx

#### 使用说明

1. 只是保证安装正常，进入后台正常，其他还没改
2. xxxx
3. xxxx

#### 扩展计划

1. 打算扩展几个插件
- Nop.Plugin.Payments.AliPay   ：支付宝支付插件
- Nop.Plugin.Payments.WeChatPay：微信支付插件
- Nop.Plugin.ExternalAuth.QQ ：  QQ登录插件
- Nop.Plugin.ExternalAuth.WeChat：微信支付插件
- Nop.Plugin.SMS.Aliyun：   阿里云短信发送插件。[参考我原来在官网的4.0版插件](https://www.nopcommerce.com/p/3191/noppluginsmsaliyun.aspx)
- Nop.Plugin.Widgets.Notice: 基于 SiginalR的通知插件，会员注册，下订单，退款等操作通知管理员
- Nop.Plugin.Widgets.OnlineChat： 基于 SiginalR的IM插件，会员可以直接与管理员聊天

2. 使用 vue 临时做了个 手机端的主题，这里需要 修改源程序 启用 CORS，还可以做个 中间件，如果是手机端，直接输出 vue打包的 html文件。

![vue 客户端主题](https://gitee.com/husb/nop42formysql/raw/master/Presentation/Nop.Web/wwwroot/m/vue_client_index.png)
![vue 客户端主题](https://gitee.com/husb/nop42formysql/raw/master/Presentation/Nop.Web/wwwroot/m/vue_client_category.png)

如果您感觉以上插件有用，请我喝杯咖啡Please take a cup of coffee：

![微信支付宝支付](https://gitee.com/husb/nop42formysql/raw/master/Presentation/Nop.Web/wwwroot/images/alipay_weixin600x470q.jpg "请使用微信或者支付宝扫码")

3.仿淘宝皮肤主题效果
- 仿淘宝主题
![仿淘宝主题](https://gitee.com/husb/nop42formysql/raw/master/Presentation/Nop.Web/Themes/taobao/preview.jpg "仿淘宝主题")

4. 我的QQ 954219492， 大家一起来扩展
5. 

## 本人承接各类 页面切图和网页设计工作

需要外包 `页面切图和网页设计` 任务的小伙伴可以联系我！！！ QQ: 954219492
